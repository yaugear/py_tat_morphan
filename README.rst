Morphological Analyser of Tatar language
========================================

Morphological Parser of Tatar language. Uses HFST-tool.
Web form which uses this tool: http://tatmorphan.pythonanywhere.com/


To Install:
-----------

$ pip install py_tat_morphan


or

$ git clone https://yaugear@bitbucket.org/yaugear/py_tat_morphan.git

$ cd py_tat_morphan

$ python setup.py install


To Install CHFST (Optional for Python 2.7):
-----------------------------------------

CHFST is a standalone package for performing transducer lookup
(transformation) using a specialised file format. The format is fast,
but the pure Python implementation is not very fast, so this package provides a fast C++ implementation with SWIG bindings.


$ cd CHFST

$ swig -c++ -python hfst_lookup.i

$ python setup.py build_ext --inplace

$ python setup.py install



To Use Lookup:
--------------

$ tat_morphan_lookup


To Process Plain Text:
----------------

$ tat_morphan_process_text <filename>



To Process Whole Folder:
------------------------

$ tat_morphan_process_folder <path_from>


or

$ tat_morphan_process_folder <path_from> <path_to>


Note: if you do not provide <path_to>, programm puts analyzed texts into folder near initial with '_analyzed' postfix. Eg, if <path_from>='/home/ramil/mytexts/', then <path_to>='/home/ramil/mytexts_analyzed/'.


To Start Morphan as a Service (in background):
---------------------------------------------

To start server (written in Flask):

$ tat_morphan_start_server

Note: to start with some other port, just add port number after `tat_morphan_start_server`.


To start in background:

$ tat_morphan_start_server &


It will start service running on http://127.0.0.1:5000/ and will accept POST-request to parse token as:

$ curl -H "Content-type: application/json" -X POST http://127.0.0.1:5000/api/token/ -d '{"token":"алма"}'


or to parse whole plain text as:

$ curl -H "Content-type: application/json" -X POST http://127.0.0.1:5000/api/text/ -d '{"text":"Без урманга барабыз"}'


Also available GET-request for words:

$ curl http://127.0.0.1:5000/api/token/алма

Note: logs of service are written to /tmp/tat_morphan_server.log


To Use as Python Module:
------------------------

>>> from py_tat_morphan.morphan import Morphan
>>> morphan = Morphan()
>>> print(morphan.analyse('урманнарга'))
урман+N+PL(ЛАр)+DIR(ГА);
>>> print(morphan.lemma('урманнарга'))
[u'\u0443\u0440\u043c\u0430\u043d']
>>> print(morphan.pos('урманнарга'))
[u'N']
>>> print(morphan.process_text('Без урманга барабыз.'))
Без
без+N+Sg+Nom;без+PN;
урманга
урман+N+Sg+DIR(ГА);
барабыз
бар+V+PRES(Й)+1PL(бЫз);
.
Type1
>>> print(morphan.analyse_text('Без урманга барабыз.'))
[[(u'\u0411\u0435\u0437', u'\u0431\u0435\u0437+N+Sg+Nom;\u0431\u0435\u0437+PN;'), (u'\u0443\u0440\u043c\u0430\u043d\u0433\u0430', u'\u0443\u0440\u043c\u0430\u043d+N+Sg+DIR(\u0413\u0410);'), (u'\u0431\u0430\u0440\u0430\u0431\u044b\u0437', u'\u0431\u0430\u0440+V+PRES(\u0419)+1PL(\u0431\u042b\u0437);'), (u'.', 'Type1')]]
>>> print(morphan.disambiguate_text('Язгы ташуларда көймә йөздерәбез.'))
[[(u'\u042f\u0437\u0433\u044b', u'\u044f\u0437\u0433\u044b+Adj;'), (u'\u0442\u0430\u0448\u0443\u043b\u0430\u0440\u0434\u0430', u'\u0442\u0430\u0448\u0443+N+PL(\u041b\u0410\u0440)+LOC(\u0414\u0410);\u0442\u0430\u0448\u044b+V+VN_1(\u0443/\u04af/\u0432)+PL(\u041b\u0410\u0440)+LOC(\u0414\u0410);'), (u'\u043a\u04e9\u0439\u043c\u04d9', u'\u043a\u04e9\u0439\u043c\u04d9+N+Sg+Nom'), (u'\u0439\u04e9\u0437\u0434\u0435\u0440\u04d9\u0431\u0435\u0437', u'\u0439\u04e9\u0437+V+CAUS(\u0414\u042b\u0440)+PRES(\u0419)+1PL(\u0431\u042b\u0437);\u0439\u04e9\u0437\u0434\u0435\u0440+V+PRES(\u0419)+1PL(\u0431\u042b\u0437);'), (u'.', 'Type1')]]


To test:
--------

$ python setup.py test


Release Notes:
--------------

https://bitbucket.org/yaugear/py_tat_morphan/wiki/Release%20Notes


Performance Test:
-----------------

https://bitbucket.org/yaugear/py_tat_morphan/wiki/Performance%20Test


For feedback:
-------------

ramil.gata@gmail.com
