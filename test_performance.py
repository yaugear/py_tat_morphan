import os
import time
from py_tat_morphan.morphan import Morphan, to_text


morphan = Morphan()


TEXTS = '/media/yaugear/BackupData1/tatcorp/IPS/repo/TEXTS/'
SAVE_TO = '/media/yaugear/BackupData1/tatcorp/IPS/repo/TEXTS/analysed'

def process_text(inpfile, outpfile):
    t0 = time.time()
    with open(inpfile, 'rb') as inpstream, open(outpfile, 'wb') as outpstream:
        text = inpstream.read().decode('utf-8')
        t1 = time.time()
        dt1 = t1 - t0
        tokens_count, utokens, sentenes_count, sentences = morphan.analyse_text(text)
        result = to_text(sentences)
        t2 = time.time()
        dt2 = t2 - t1
        outpstream.write(result.encode('utf-8'))
        t3 = time.time()
        dt3 = t3 - t2

    return tokens_count, sentenes_count, dt1, dt2, dt3, dt1 + dt2 + dt3, utokens

if __name__ == '__main__':
    for textfile in sorted(os.listdir(TEXTS)):
        if os.path.isdir(os.path.join(TEXTS, textfile)):
            continue
        # print(textfile)
        res = process_text(os.path.join(TEXTS, textfile), os.path.join(SAVE_TO, textfile))
        print('%s\t%s\t%s\t%.2fs\t%.2f tokps\t%.2f tokps' % (textfile, res[0], res[6], res[5], (res[0] / res[5]), (res[6] / res[5])))

        # print(textfile + ' ' + '\t'.join([str(el) for el in res]) + "=%.2f tokens per second" % (res[0] / res[5]))
